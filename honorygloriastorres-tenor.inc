\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 2/4
		\clef "treble_8"
		\key d \major

		R2*3  |
		r4. a, 8  |
%% 5
		d 4. a, 8  |
		e 4. a, 8  |
		fis 2 ~  |
		fis 4 r8 d  |
		b 4. b 8  |
%% 10
		\times 2/3 { a 4 g fis }  |
		a 2 ~  |
		a 4 r8 a  |
		g 4. g 8  |
		a 4. a 8  |
%% 15
		fis 2 ~  |
		fis 2  |
		r4 g  |
		g 4 g  |
		fis 2 ~  |
%% 20
		fis 2  |
		R2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		Ho -- nor y glo -- "ria a" ti,
		Se -- ñor, a ti el ho -- nor.
		Ho -- nor y glo -- "ria a" ti,
		Se -- ñor, Je -- sús.
	}
>>
