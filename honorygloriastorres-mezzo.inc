\context Staff = "mezzo" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzo" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 2/4
		\clef "treble"
		\key d \major

		R2*3  |
		r4. a 8  |
%% 5
		d' 4. a 8  |
		e' 4. a 8  |
		d' 2 ~  |
		d' 4 r8 d'  |
		b 4. d' 8  |
%% 10
		\times 2/3 { d' 4 d' d' }  |
		cis' 2 ~  |
		cis' 4 r8 cis'  |
		b 4. cis' 8  |
		d' 4. cis' 8  |
%% 15
		b 2 ~  |
		b 2  |
		r4 d'  |
		d' 4 cis'  |
		a 2 ~  |
%% 20
		a 2  |
		R2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzo" {
		Ho -- nor y glo -- "ria a" ti,
		Se -- ñor, a ti el ho -- nor.
		Ho -- nor y glo -- "ria a" ti,
		Se -- ñor, Je -- sús.
	}
>>
