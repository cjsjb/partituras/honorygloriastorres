\context ChordNames 
	\chords {
		\set chordChanges = ##t
		% intro
		d2 a2 g2 a2

		% honor y gloria a ti...
		d2 a2 b2:m b2:m
		g2 d2 a2 a2
		g2 a2 b2:m b2:m
		g2 a2 d2 d2
		d2
	}
