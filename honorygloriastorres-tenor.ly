% This LilyPond file was generated by Rosegarden 1.7.3
\version "2.10.0"
% point and click debugging is disabled
#(ly:set-option 'point-and-click #f)
\header {
	title = "Honor y gloria"
	composer = "Sergio Armando Torres Gómez"
	tagline = "Coro Juvenil San Juan Bosco"
	instrument = "Tenor"
}
#(set-global-staff-size 20)
#(set-default-paper-size "letter")
\paper {
	#(define line-width (* 7 in))
	print-first-page-number = ##t
	ragged-bottom = ##t
	first-page-number = 1
}
global = {
	\time 2/4
}
globalTempo = {
	\tempo 4 = 116
}
\score {
	<<
		% force offset of colliding notes in chords:
		\override Score.NoteColumn #'force-hshift = #1.0

		\include "honorygloriastorres-acordes.inc"
		\include "honorygloriastorres-tenor.inc"
	>>

	\layout {
		\context {
			\RemoveEmptyStaffContext
		}
	}
}
