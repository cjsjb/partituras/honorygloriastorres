\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 2/4
		\clef "treble"
		\key d \major

		R2*3  |
		r4. a 8  |
%% 5
		d' 4. a 8  |
		e' 4. a 8  |
		fis' 2 ~  |
		fis' 4 r8 d'  |
		g' 4. g' 8  |
%% 10
		\times 2/3 { fis' 4 e' d' }  |
		e' 2 ~  |
		e' 4 r8 e'  |
		d' 4. d' 8  |
		e' 4. e' 8  |
%% 15
		d' 2 ~  |
		d' 2  |
		r4 d'  |
		d' 4 cis'  |
		d' 2 ~  |
%% 20
		d' 2  |
		R2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-soprano" {
		Ho -- nor y glo -- "ria a" ti,
		Se -- ñor, a ti el ho -- nor.
		Ho -- nor y glo -- "ria a" ti,
		Se -- ñor, Je -- sús.
	}
>>
